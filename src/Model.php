<?php
namespace Apitest;

use Illuminate\Database\Eloquent\Model as BaseModel;

abstract class Model extends BaseModel
{
    public $timestamps = false;
}
