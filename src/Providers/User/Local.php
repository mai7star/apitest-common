<?php
namespace Apitest\Providers\User;

class Local extends \Apitest\Providers\User
{
    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array  $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        $logger = app('log');

        $token_id = array_get($credentials, 'token_id');
        $logger->info("Token ID: {$token_id}");

        $token = \App\Models\Token::find($token_id);
        if(is_null($token)){
            $logger->info("Token ID is not found.");
            return null;
        }

        if($token->expired_at->lt(\Carbon\Carbon::now())){
            $logger->info("Token ID is expired.");
            return null;
        }

        $user = $token->user;
        if(is_null($user)){
            $logger->info("No User for the Token ID.");
            return null;
        }

        if($user->enabled != true){
            $logger->info("The User is not enabled.");
            return null;
        }

        $logger->info("User ID: {$user->id}.");
        return $user;
    }
}