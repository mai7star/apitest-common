<?php
namespace Apitest\Providers\User;

class Remote extends \Apitest\Providers\User
{
    private $client = null;

    public function __construct(\GuzzleHttp\Client $client)
    {
        $this->client = $client;
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array  $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        $logger = app('log');

        $token_id = array_get($credentials, 'token_id');
        $logger->info("Token ID: {$token_id}");

        $logger->info("Start Request..");
        $result = $this->client->request('get', '/v1/users/0', [
            'http_errors' => false,
            'headers' => ['Authorization' => "Bearer {$token_id}"]
        ]);

        $code = $result->getStatusCode();
        $logger->info("Result: {$code}.");
        if($code != 200){
            return null;
        }

        $data = json_decode($result->getBody()->getContents(), true);
        if(is_null($data)){
            $logger->notice("Invalid json format.");
            return null;
        }

        $user = new \App\Models\User($data);

        $logger->info("User ID: {$user->id}.");
        return $user;
    }
}