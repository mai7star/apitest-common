<?php
namespace Apitest\Providers\User;

class Credential extends \Apitest\Providers\User
{
    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array  $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        $logger = app('log');

        $domain = array_get($credentials, 'domain', 'local');
        $login = array_get($credentials, 'login');
        $password = array_get($credentials, 'password');

        $logger->info("Domain: {$domain}, Login: {$login}");
        $user = \App\Models\User::where('domain', $domain)->where('login', $login)->first();
        if(is_null($user)){
            $logger->info("Domain: {$domain}, Login: {$login}: not found.");
            return null;
        }

        $hash = app('hash');
        if($hash->check($password, $user->password) != true){
            $logger->info("Domain: {$domain}, Login: {$login}: password missmatch.");
            return null;
        }

        if($hash->needsRehash($user->password)){
            $logger->info("Domain: {$domain}, Login: {$login}: password hash is weeek. re-generating.");

            $user->password = $password;
            $user->save();
        }

/*
        if($device_id != ''){
            \App\Models\Device::create(['id' => $device_id, 'user_id' => $user->id]);
        }
*/
        return $user;
    }
}