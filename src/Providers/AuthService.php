<?php
namespace Apitest\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;

class AuthService extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->app['auth']->viaRequest('api', function($request){
            $guard = new \Apitest\Guards\Token();
            $guard->validate(['token_id' => $request->bearerToken()]);

            return $guard->user();
        });

        $this->app['auth']->extend('credential', function(){
            return new \Apitest\Guards\Credential();
        });
        $this->app['config']['auth.guards.credential'] = ['driver' => 'credential'];


        $gate->define('admin', function($user){
            return $user->hasRole('admin');
        });

        $gate->define('role', function($user, string $role){
            return $user->hasRole($role);
        });
    }
}
