<?php
namespace Apitest;

class Event
{
    static public function push(int $user_id, string $name, array $args = [])
    {
        $url = env('ACCOUNT_URL');
        if(is_null($url)){
            \App\Models\Event::create([
                'user_id' => $user_id,
                'name' => $name,
                'args' => $args
            ]);

        } else {
            $client = new \GuzzleHttp\Client([
                'base_uri' => env('ACCOUNT_URL'),
                'verify' => env('VERIFY_SSL', true)
            ]);

            $token_id = env('SERVICE_TOKEN');
            $result = $client->request('post', '/v1/events', [
                'http_errors' => false,
                'headers' => ['Authorization' => "Bearer {$token_id}"],
                'json' => [
                    'user_id' => $user_id,
                    'name' => $name,
                    'args' => $args,
                ]
            ]);
        }
    }
}
