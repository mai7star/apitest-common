<?php
namespace Apitest\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthenticationException::class,
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(\Exception $e)
    {
        if($this->shouldReport($e)){
            $airbrake_url = env('AIRBRAKE_URL');
            $project_key = env('AIRBRAKE_KEY');
            if($airbrake_url == '' || $project_key == ''){
                return;
            }

            (new \Airbrake\Notifier([
                    'host' => $airbrake_url,
                    'environment' => env('APP_ENV', ''),
                    'projectId' => -1,
                    'projectKey' => $project_key
                ]))->notify($e);
        }

        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, \Exception $e)
    {
        if($e instanceof AuthenticationException){
            return response()->json(null, 401)
                        ->header('WWW-Authenticate', sprintf("Bearer error='%s'", $e->getMessage()));
        }

        if($e instanceof AuthorizationException){
            return response()->json(null, 403)
                        ->header('WWW-Authenticate', sprintf("Bearer error='%s'", $e->getMessage()));
        }

        if($e instanceof ModelNotFoundException){
            return response()->json(null, 404);
        }

        return parent::render($request, $e);
    }
}
