<?php
namespace Apitest\Models;

use Apitest\Model;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract
{
    use Authenticatable,
        Authorizable;

    public function hasRole(string $role)
    {
        if(env('ACCOUNT_URL') == ''){
            $roles = $this->roles->pluck('role')->toArray();
        } else {
            $roles = $this->roles;
        }

        return in_array($role, $roles);
    }

    public function getEnabledAttribute()
    {
        return true;
    }
}
