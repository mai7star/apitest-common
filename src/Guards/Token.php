<?php
namespace Apitest\Guards;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;

class Token implements Guard
{
    private $providor = null;

    private $user = null;

    /**
     * Create a new authentication guard.
     *
     * @return void
     */
    public function __construct()
    {
        $account_host = env('ACCOUNT_URL');
        if($account_host == ''){
            $this->providor = new \Apitest\Providers\User\Local();

        } else {
            $client = new \GuzzleHttp\Client(['base_uri' => $account_host, 'verify' => env('VERIFY_SSL', true)]);
            $this->providor = new \Apitest\Providers\User\Remote($client);
        }
    }

    /**
     * Determine if the current user is authenticated.
     *
     * @return bool
     */
    public function check()
    {
        return !$this->guest();
    }

    /**
     * Determine if the current user is a guest.
     *
     * @return bool
     */
    public function guest()
    {
        return is_null($this->user);
    }

    /**
     * Get the currently authenticated user.
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function user()
    {
        return $this->user;
    }

    /**
     * Get the ID for the currently authenticated user.
     *
     * @return int|null
     */
    public function id()
    {
        return is_null($this->user) ? null : $this->user->id;
    }

    /**
     * Validate a user's credentials.
     *
     * @param  array  $credentials
     * @return bool
     */
    public function validate(array $credentials = [])
    {
        $this->user = $this->providor->retrieveByCredentials($credentials);

        return $this->check();
    }

    /**
     * Set the current user.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @return void
     */
    public function setUser(Authenticatable $user)
    {
        $this->user = $user;

        return $this;
    }
}
