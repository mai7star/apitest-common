<?php
namespace App\Models;

use Apitest\Model;

class Token extends Model
{
    public $incrementing = false;
    protected $guarded = [];
    protected $dates = ['expired_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}