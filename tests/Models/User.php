<?php
namespace App\Models;

use Apitest\Models\User as BaseUser;

class User extends BaseUser
{
    protected $guarded = [];

    public function roles()
    {
        return $this->hasMany(Role::class);
    }

    public function setPasswordAttribute(string $password)
    {
        $this->attributes['password'] = app('hash')->make($password);
    }
}