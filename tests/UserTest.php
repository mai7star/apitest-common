<?php
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    static public function forHasRole()
    {
        return [
            [true, 'role1'],
            [true, 'role2'],
            [false, 'role3'],
        ];
    }

    /**
     * @dataProvider forHasRole
     *
     */
    public function testHasRoleRemote($expected, $role)
    {
        $user = new \Apitest\Models\User();
        $user->roles = ['role1', 'role2'];

        $this->assertSame($expected, $user->hasRole($role));
    }

    /**
     * @dataProvider forHasRole
     *
     */
    public function testHasRoleLocal($expected, $role)
    {
        putenv('ACCOUNT_URL=');

        $user = new \Apitest\Models\User();
        $user->roles = collect([
            ['role' => 'role1'],
            ['role' => 'role2']
        ]);

        $this->assertSame($expected, $user->hasRole($role));
    }
}
