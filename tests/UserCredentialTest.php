<?php

class UserCredentialTest extends WithModelTestCase
{
    protected $setup_tables = [
        'users' => 'id integer primary key, domain text, login text, password text, name text'
    ];

    protected $setup_data = [
        'users' => [
            ['id' => 1, 'domain' => 'domain1', 'login' => 'user1', 'password' => '$2y$10$olu1BeNDJAD.6J7bvGZVv.ZRYoZvZWraqFbRgrXZl8hZotE2abzry', 'name' => 'name1'],
            ['id' => 2, 'domain' => 'domain2', 'login' => 'user1', 'password' => '$2y$10$V2ilVT7TYQjVoQJq0wDf4u50zR9rl0puZ/ucX6/LuMCHvfyQZSJnC', 'name' => 'name2'],
            ['id' => 3, 'domain' => 'domain1', 'login' => 'user2', 'password' => '$2y$10$m9GEAlrfGfKp2IVruN4x2e3sK.OCsE151YQBuuaXrxgaQNxb11beG', 'name' => 'name3'],
            ['id' => 4, 'domain' => 'domain2', 'login' => 'user2', 'password' => 'sa/4pHNdnrm6Q', 'name' => 'name4'],
        ]
    ];

    static public function forTestRetrieveByCredentials()
    {
        return [
            [1, 'domain1', 'user1', 'abcd'],
            [2, 'domain2', 'user1', 'ABCD'],
            [3, 'domain1', 'user2', '0123'],
            [null, 'domain1', 'user1', 'ABCD'],
            [4, 'domain2', 'user2', 'abcd'],
        ];
    }

    /**
     * @dataProvider forTestRetrieveByCredentials
     *
     */
    public function testRetrieveByCredentials($expected, $domain, $login, $password)
    {
        $resolver = new \Apitest\Providers\User\Credential();
        $user = $resolver->retrieveByCredentials(['domain' => $domain, 'login' => $login, 'password' => $password]);

        if(is_null($expected)){
            $this->assertNull($user);
            return;
        }

        $this->assertSame($expected, $user->id);
        $this->assertRegExp('/^\$2y\$10\$/', $user->password);
    }
}
