<?php

use \Illuminate\Database\SQLiteConnection;
use \Illuminate\Database\ConnectionResolver;
use \Illuminate\Database\Eloquent\Model;

class WithModelTestCase extends PHPUnit_Extensions_Database_TestCase
{
    private $pdo = null;

    protected $setup_tables = [];
    protected $setup_data = [];

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->pdo = new PDO('sqlite::memory:');
        foreach($this->setup_tables as $name => $cols){
            $this->pdo->exec("create table {$name} ({$cols})");
        }
    }

    public function getConnection()
    {
        return $this->createDefaultDBConnection($this->pdo, ':memory:');
    }

    public function getDataSet()
    {
        return $this->createArrayDataSet($this->setup_data);
    }

    protected function setUp()
    {
        parent::setUp();

        $db = new SQLiteConnection($this->pdo);

        $resolver = new ConnectionResolver(['db' => $db]);
        $resolver->setDefaultConnection('db');

        Model::setConnectionResolver($resolver);
    }
}
