<?php

class UserLocalTest extends WithModelTestCase
{
    protected $setup_tables = [
        'tokens' => 'id text primary key, user_id text, address text, expired_at text',
        'users' => 'id integer primary key, name text'
    ];

    protected $setup_data = [
        'tokens' => [
            ['id' => 'a1', 'user_id' => 1, 'address' => '127.0.0.1', 'expired_at' => '2099-12-31 23:59:59'],
            ['id' => 'a2', 'user_id' => 1, 'address' => '127.0.0.1', 'expired_at' => '2099-12-31 23:59:59'],
            ['id' => 'b', 'user_id' => 2, 'address' => '127.0.0.1', 'expired_at' => '2099-12-31 23:59:59'],
            ['id' => 'c', 'user_id' => 3, 'address' => '127.0.0.1', 'expired_at' => '2000-01-01 00:00:00'],
            ['id' => 'd', 'user_id' => 4, 'address' => '127.0.0.1', 'expired_at' => '2099-12-31 23:59:59']
        ],
        'users' => [
            ['id' => 1, 'name' => 'name1'],
            ['id' => 2, 'name' => 'name2'],
            ['id' => 3, 'name' => 'name3']
        ]
    ];

    static public function forTestRetrieveByCredentials()
    {
        return [
            [null, ''],
            [1, 'a1'],
            [1, 'a2'],
            [2, 'b'],
            [null, 'c'],
            [null, 'd'],
            [null, 'e'],
        ];
    }

    /**
     * @dataProvider forTestRetrieveByCredentials
     *
     */
    public function testRetrieveByCredentials($expected, $token_id)
    {
        $resolver = new \Apitest\Providers\User\Local();
        $user = $resolver->retrieveByCredentials(['token_id' => $token_id]);

        if(is_null($expected)){
            $this->assertNull($user);
            return;
        }

        $this->assertSame($expected, $user->id);
    }
}
