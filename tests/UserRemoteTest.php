<?php
use PHPUnit\Framework\TestCase;

class UserRemoteTest extends TestCase
{
    static public function forTestRetrieveByCredentials()
    {
        return [
            [null,  '', 200, [], ''],
            [   1, 'a', 200, ['Content-Type' => 'application/json'], json_encode(['id' => 1, 'roles' => ['role1', 'role2']])],
            [null, 'a', 400, [], ''],
            [null, 'a', 200, ['Content-Type' => 'application/json'], 'invalid json format'],
        ];
    }

    /**
     * @dataProvider forTestRetrieveByCredentials
     *
     */
    public function testRetrieveByCredentials($expected, $token_id, $code, $header, $body)
    {
        $mock = new \GuzzleHttp\Handler\MockHandler([
            new \GuzzleHttp\Psr7\Response($code, $header, $body)
        ]);

        $client = new \GuzzleHttp\Client([
            'handler' => \GuzzleHttp\HandlerStack::create($mock)
        ]);

        $remote = new \Apitest\Providers\User\Remote($client);
        $user = $remote->retrieveByCredentials(['token_id' => $token_id]);

        if(is_null($expected)){
            $this->assertNull($user);
            return;
        }

        $this->assertEquals($expected, $user->id);
    }
}