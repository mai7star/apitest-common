<?php
require_once __DIR__.'/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    echo ".env for test is missing.";
    exit(1);
}

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

require_once 'Models/Token.php';
require_once 'Models/User.php';

require_once 'TestCase/WithModel.php';

app()->bind('log', function(){
    return new Logger('lumen', [
        new StreamHandler('php://stderr', Logger::CRITICAL)
    ]);
});

app()->bind('hash', function () {
    return new \Illuminate\Hashing\BcryptHasher();
});